package com.gooutandroid01.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gooutandroid01.R;
import com.gooutandroid01.utils.ImageLoader;
import com.pkmmte.view.CircularImageView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ListActivity extends ActionBarActivity {

    private ImageLoader imgLoader;

    private ArrayList<JSONObject> data = null;

    private CircularImageView profilePic;

    private TextView nameText;

    private TextView emailText;

    private ListView photosList;

    private String api_key = "0e2b6aaf8a6901c264acb91f151a3350";

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        imgLoader = new ImageLoader(this);

        String profileUrl = "https://graph.facebook.com/%s/picture?height=110&width=110";

        SharedPreferences sharedpreferences = getSharedPreferences("FazINOVA Prefs", Context.MODE_PRIVATE);

        profilePic  = (CircularImageView) findViewById(R.id.profile_image);
        nameText    = (TextView) findViewById(R.id.name);
        emailText   = (TextView) findViewById(R.id.email);
        photosList  = (ListView) findViewById(R.id.listview);

        String userId = sharedpreferences.getString(("id"), null);
        String userName = sharedpreferences.getString(("name"), null);
        String userMail = sharedpreferences.getString(("email"), null);

        profileUrl = String.format(profileUrl, userId);

        imgLoader.DisplayImage(profileUrl, profilePic);

        nameText.setText(userName);
        emailText.setText(userMail);

        dialog = ProgressDialog.show(this, "", "Carregando...", true);

        new getPhotosByTag().execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class ListAdapter extends BaseAdapter {

        Context context;
        ArrayList<JSONObject> data;
        private LayoutInflater inflater = null;

        public ListAdapter(Context context, ArrayList<JSONObject> data) {
            // TODO Auto-generated constructor stub
            this.context = context;
            this.data = data;
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            View vi = convertView;
            if (vi == null){
                vi = inflater.inflate(R.layout.flickr_row_cell, null);
            }

            JSONObject photo_obj = data.get(position);

            TextView photo_title        = (TextView) vi.findViewById(R.id.photo_title);
            ImageView photo_thumb       = (ImageView) vi.findViewById(R.id.photo_thumb);

            try {
                photo_title.setText(photo_obj.getString("title"));

                String photo_id = photo_obj.getString("id");
                String secret = photo_obj.getString("secret");
                String server = photo_obj.getString("server");
                String url = String.format("https://farm1.staticflickr.com/%s/%s_%s_s.jpg", server,photo_id,secret);

                imgLoader.DisplayImage(url, photo_thumb);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return vi;
        }
    }

    class getPhotosByTag extends AsyncTask<String, String, String> {

        JSONObject finalResult;

        String tag = "movies";

        String url = "https://api.flickr.com/services/rest/?format=json&sort=random&method=flickr.photos.search&tags=%s&api_key=%s";

        protected String doInBackground(String... urls) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(String.format(url, tag, api_key));
            BufferedReader reader;
            try {
                HttpResponse response=httpclient.execute(httpget);
                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                StringBuilder builder = new StringBuilder();
                for (String line = null; (line = reader.readLine()) != null;) {
                    builder.append(line).append("\n");
                }

                int firstKey = builder.toString().indexOf("{");
                int lastKey = builder.toString().lastIndexOf(")");
                String flickrFormat = builder.toString().substring(firstKey,lastKey);

                JSONTokener tokener = new JSONTokener(flickrFormat);

                try {
                    finalResult = new JSONObject(tokener);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String feed) {

            try {
                JSONObject results = finalResult.getJSONObject("photos");
                JSONArray photos_array = results.getJSONArray("photo");

                data = new ArrayList<>();

                for(int i = 0 ; i<photos_array.length() ; i++) {
                    JSONObject photo_obj = photos_array.getJSONObject(i);
                    data.add(photo_obj);
                }

                ListAdapter myAdapter = new ListAdapter(ListActivity.this, data);
                photosList.setAdapter(myAdapter);

                photosList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(ListActivity.this, PhotoDetailActivity.class);
                        i.putExtra("chosenPhoto", data.get(position).toString());
                        startActivity(i);
                    }
                });

            } catch (JSONException e) {
                Toast.makeText(ListActivity.this,""+e,
                        Toast.LENGTH_SHORT).show();
            }

            // RELOAD
            dialog.dismiss();
        }

    }
}
