package com.gooutandroid01.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gooutandroid01.R;
import com.gooutandroid01.utils.ImageLoader;
import com.pkmmte.view.CircularImageView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PhotoDetailActivity extends ActionBarActivity {

    private ImageLoader imgLoader;

    private ArrayList<JSONObject> data = null;

    private ImageView moviePic;
    private TextView photo_title;
    private CircularImageView author_icon;
    private TextView author_username;
    private TextView comments_count;

    private ListView commentsList;

    private JSONObject photo_obj;

    private String photo_id;

    private String api_key = "0e2b6aaf8a6901c264acb91f151a3350";

    private String author_icon_url = "https://farm3.staticflickr.com/%s/buddyicons/%s.jpg";

    private String photo_comments_url = "https://api.flickr.com/services/rest/?format=json&sort=random&method=flickr.photos.comments.getList&photo_id=%s&api_key=%s";

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imgLoader = new ImageLoader(this);

        moviePic            = (ImageView) findViewById(R.id.photo_banner);
        photo_title         = (TextView) findViewById(R.id.photo_title);
        author_icon         = (CircularImageView) findViewById(R.id.author_profile_image);
        author_username     = (TextView) findViewById(R.id.author_name);
        comments_count      = (TextView) findViewById(R.id.comments_count);
        commentsList        = (ListView) findViewById(R.id.listview_comments);

        Bundle extras = getIntent().getExtras();

        if(extras!=null) {
            try {
                photo_obj = new JSONObject(extras.getString("chosenPhoto"));

                photo_id = photo_obj.getString("id");
                String secret = photo_obj.getString("secret");
                String server = photo_obj.getString("server");
                String url = String.format("https://farm1.staticflickr.com/%s/%s_%s.jpg", server,photo_id,secret);

                imgLoader.DisplayImage(url, moviePic);

                photo_title.setText(photo_obj.getString("title"));

                dialog = ProgressDialog.show(this, "", "Carregando...", true);

                new getPhotoInfo().execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id){
            case android.R.id.home:
                // API 5+ solution
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class ListAdapter extends BaseAdapter {

        Context context;
        ArrayList<JSONObject> data;
        private LayoutInflater inflater = null;

        public ListAdapter(Context context, ArrayList<JSONObject> data) {
            // TODO Auto-generated constructor stub
            this.context = context;
            this.data = data;
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            View vi = convertView;
            if (vi == null){
                vi = inflater.inflate(R.layout.comment_row_cell, null);
            }

            JSONObject comment_obj = data.get(position);

            TextView comment_text                       = (TextView) vi.findViewById(R.id.comment_text);
            CircularImageView author_profile_image      = (CircularImageView) vi.findViewById(R.id.author_profile_image);

            try {
                comment_text.setText(comment_obj.getString("_content"));
                imgLoader.DisplayImage(String.format(author_icon_url, comment_obj.getString("iconserver"), comment_obj.getString("author")), author_profile_image);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return vi;
        }
    }

    class getPhotoInfo extends AsyncTask<String, String, String> {

        JSONObject finalResult;

        String url = "https://api.flickr.com/services/rest/?format=json&sort=random&method=flickr.photos.getInfo&photo_id=%s&api_key=%s";

        protected String doInBackground(String... urls) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(String.format(url, photo_id, api_key));
            BufferedReader reader;
            try {
                HttpResponse response=httpclient.execute(httpget);
                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                StringBuilder builder = new StringBuilder();
                for (String line = null; (line = reader.readLine()) != null;) {
                    builder.append(line).append("\n");
                }

                int firstKey = builder.toString().indexOf("{");
                int lastKey = builder.toString().lastIndexOf(")");
                String flickrFormat = builder.toString().substring(firstKey,lastKey);

                JSONTokener tokener = new JSONTokener(flickrFormat);

                try {
                    finalResult = new JSONObject(tokener);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String feed) {

            try {
                JSONObject results = finalResult.getJSONObject("photo");

                JSONObject author_obj = results.getJSONObject("owner");

                author_username.setText(author_obj.getString("username"));

                imgLoader.DisplayImage(String.format(author_icon_url, author_obj.getString("iconserver"), author_obj.getString("nsid")),author_icon);

                // RELOAD
                dialog.dismiss();
                dialog = ProgressDialog.show(PhotoDetailActivity.this, "", "Carregando...", true);
                new getPhotoComments().execute();

            } catch (JSONException e) {
                Toast.makeText(PhotoDetailActivity.this,""+e,
                        Toast.LENGTH_SHORT).show();

                // RELOAD
                dialog.dismiss();
            }
        }

    }

    class getPhotoComments extends AsyncTask<String, String, String> {

        JSONObject finalResult;

        protected String doInBackground(String... urls) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(String.format(photo_comments_url, photo_id, api_key));
            BufferedReader reader;
            try {
                HttpResponse response=httpclient.execute(httpget);
                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                StringBuilder builder = new StringBuilder();
                for (String line = null; (line = reader.readLine()) != null;) {
                    builder.append(line).append("\n");
                }

                int firstKey = builder.toString().indexOf("{");
                int lastKey = builder.toString().lastIndexOf(")");
                String flickrFormat = builder.toString().substring(firstKey,lastKey);

                JSONTokener tokener = new JSONTokener(flickrFormat);

                try {
                    finalResult = new JSONObject(tokener);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String feed) {

            try {
                JSONObject results = finalResult.getJSONObject("comments");

                if(results.has("comment")) {
                    JSONArray comments_array = results.getJSONArray("comment");

                    comments_count.setText(String.format("%s Comments", comments_array.length()));

                    data = new ArrayList<>();

                    for (int i = 0; i < comments_array.length(); i++) {
                        data.add(comments_array.getJSONObject(i));
                    }

                    ListAdapter myAdapter = new ListAdapter(PhotoDetailActivity.this, data);
                    commentsList.setAdapter(myAdapter);
                }else{
                    comments_count.setText("0 Comments");
                }

            } catch (JSONException e) {
                Toast.makeText(PhotoDetailActivity.this,""+e,
                        Toast.LENGTH_SHORT).show();
            }

            // RELOAD
            dialog.dismiss();
        }

    }
}
